package net.one97.paytm.imagepoc.scripts.filefilter;

import java.io.File;
import java.io.FileFilter;

public class ManifestFileFilter implements FileFilter {

    @Override
    public boolean accept(File file) {

        if (file.isDirectory())
            return false;

        return file.getName().equals("AndroidManifest.xml");
    }
}
