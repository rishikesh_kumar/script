package net.one97.paytm.imagepoc.scripts.filefilter;

import java.io.File;
import java.io.FileFilter;

public class JpegFilter implements FileFilter {

    @Override
    public boolean accept(File file) {

        if(file.isDirectory())
            return false;

        String name = file.getName();
        return name.endsWith(".jpg");
    }
}
