package net.one97.paytm.imagepoc.scripts.filefilter;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;

public class DefaultResourceActivityFilter extends ActivityFileFilter {

    @Override
    public boolean accept(File file) {

        boolean isActivity = super.accept(file);

        if(!isActivity)
            return false;

        try (BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(file)))) {

            String line = null;

            while ((line = reader.readLine()) != null){

                if(line.contains("attachBaseContext")){
                    return false;
                }
            }

        } catch (Exception ex) {
            return false;
        }

        return true;
    }
}
