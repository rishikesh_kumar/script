package net.one97.paytm.imagepoc.scripts.processstep;

import net.one97.paytm.imagepoc.scripts.IProcessStep;
import net.one97.paytm.imagepoc.scripts.filefilter.PngFileFilter;
import net.one97.paytm.imagepoc.scripts.image.ImageDimension;
import net.one97.paytm.imagepoc.scripts.util.Utils;
import org.apache.commons.io.IOUtils;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.*;

public class ConsolidatePngDrawableMerger implements IProcessStep {

    @Override
    public void process() throws Exception {

        String resRootFolderPath = "/Users/Ajitesh/Documents/WorkRelatedProjects/paytm-android-go/Jarvis/res/";

        String outputFolderPath = "/Users/Ajitesh/Desktop/MergedPngDrawables/";
        String backupFolderPath = "/Users/Ajitesh/Desktop/XMLReferencedDrawablesBackup/";

        File outputFolder = new File(outputFolderPath);
        if(!outputFolder.exists()){
            outputFolder.mkdir();
        }

        File backupFolder = new File(backupFolderPath);
        if(!backupFolder.exists()){
            backupFolder.mkdir();
        }


        Set<String> xmlReferencedPngNames = Utils.findAllXmlDrawableReferencedBitmaps(resRootFolderPath);
        Set<String> criticalPngNames = Utils.getCriticalPngNames();

        Set<String> notTouchPngNames = new HashSet<>();
        notTouchPngNames.addAll(xmlReferencedPngNames);
        notTouchPngNames.addAll(criticalPngNames);

        Set<String> modifiedPngNameSet = new HashSet<>();

        final String[] drawableFolderNames = new String[]{
                "drawable-hdpi", "drawable-mdpi", "drawable",
                "drawable-xhdpi", "drawable-xxhdpi", "drawable-xxxhdpi"
        };

        for (int i = drawableFolderNames.length - 1; i >= 0; i--) {

            String drawableFolderName = drawableFolderNames[i];

            String drawableFolderFullPath = resRootFolderPath + drawableFolderName + "/";

            File drawableFolder = new File(drawableFolderFullPath);
            if (!drawableFolder.exists())
                continue;

            File[] pngFiles = drawableFolder.listFiles(new PngFileFilter());

            for (File pngFile : pngFiles) {

                String pngFileName = pngFile.getName();
                String resourceName = pngFileName.substring(0, pngFileName.lastIndexOf("."));

                FileInputStream inputStream = new FileInputStream(pngFile);

                if (notTouchPngNames.contains(resourceName)) {

                    String subBackupFolderPath = backupFolderPath + drawableFolderName;

                    File subBackupFolder = new File(subBackupFolderPath);
                    if (!subBackupFolder.exists())
                        subBackupFolder.mkdir();

                    OutputStream backupOutputFileStream = new FileOutputStream(subBackupFolderPath + "/" + pngFileName);
                    IOUtils.copy(inputStream, backupOutputFileStream);


                    backupOutputFileStream.flush();
                    backupOutputFileStream.close();

                } else {

                    File outputFile = new File(outputFolderPath + pngFileName);

                    if (outputFile.exists())
                        outputFile.delete();


                    OutputStream outputStream = new FileOutputStream(outputFile);

                    IOUtils.copy(inputStream, outputStream);

                    outputStream.flush();
                    outputStream.close();

                    String pngFilePath = pngFile.getAbsolutePath();

                    ImageDimension imageDimension = ImageDimension.getImageDimension(pngFilePath);
                    BufferedImage placeHolderImage = Utils.createPlaceHolder(imageDimension);

                    Utils.saveAsPng(placeHolderImage, pngFilePath);

                    modifiedPngNameSet.add(resourceName);

                    System.out.println(pngFileName);
                }

                inputStream.close();
            }
        }

        List<String> modifiedPngNamesList = new ArrayList<>(modifiedPngNameSet);
        Collections.sort(modifiedPngNamesList);

        StringBuilder builder = new StringBuilder();

        for (String name : modifiedPngNamesList) {
            builder.append('\"');
            builder.append(name);
            builder.append("\", ");
        }

        if (builder.length() > 0) {
            builder.deleteCharAt(builder.length() - 1);
        }
    }
}
