package net.one97.paytm.imagepoc.scripts.image;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;

public class ImageDimension {

    private final int width;
    private final int height;

    public ImageDimension(int width, int height) {
        this.width = width;
        this.height = height;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public static ImageDimension getImageDimension(String path) {

        try {
            BufferedImage bufferedImage = ImageIO.read(new File(path));
            return new ImageDimension(bufferedImage.getWidth(), bufferedImage.getHeight());
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return null;
    }
}
