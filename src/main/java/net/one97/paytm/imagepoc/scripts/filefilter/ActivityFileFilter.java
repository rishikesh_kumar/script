package net.one97.paytm.imagepoc.scripts.filefilter;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;

public class ActivityFileFilter extends JavaFileFilter {

    @Override
    public boolean accept(File file) {

        boolean isJavaFile = super.accept(file);

        if (!isJavaFile)
            return false;

        try (BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(file)))) {

            String line = null;

            while ((line = reader.readLine()) != null){

                int extendsIndex = line.indexOf("extends");

                if(extendsIndex > -1){

                    int activityIndex = line.indexOf("Activity", extendsIndex + 7);

                    if(activityIndex > -1){
                        return true;
                    }
                }
            }

        } catch (Exception ex) {
        }

        return false;
    }
}
