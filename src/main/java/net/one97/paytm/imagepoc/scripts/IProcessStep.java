package net.one97.paytm.imagepoc.scripts;

public interface IProcessStep {

    void process() throws Exception;
}

