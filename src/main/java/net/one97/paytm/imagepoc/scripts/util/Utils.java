package net.one97.paytm.imagepoc.scripts.util;

import net.one97.paytm.imagepoc.scripts.filefilter.JpegFilter;
import net.one97.paytm.imagepoc.scripts.filefilter.PngFileFilter;
import net.one97.paytm.imagepoc.scripts.filefilter.WebpFileFilter;
import net.one97.paytm.imagepoc.scripts.filefilter.XmlFileFilter;
import net.one97.paytm.imagepoc.scripts.image.ImageDimension;
import org.apache.commons.io.IOUtils;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.*;
import java.nio.charset.Charset;
import java.util.List;
import java.util.*;

public final class Utils {

    public static Set<String> getFileNameWithoutExtension(Collection<String> fileNames) {

        Set<String> outputList = new HashSet<>();

        for (String fileName : fileNames) {

            String output = fileName.substring(0, fileName.lastIndexOf('.'));
            outputList.add(output);
        }

        return outputList;
    }

    public static Set<String> getFileNames(File[] files) {

        Set<String> resourceNames = new HashSet<>();

        for (File childFile : files) {

            String resourceName = childFile.getName();
            resourceNames.add(resourceName);
        }

        return resourceNames;
    }

    public static boolean isMainDrawableFolder(String fileName) {
        return (fileName.contains("drawable-") && fileName.endsWith("dpi") && !fileName.equals("drawable-xxhdpi"));
    }

    public static boolean isDrawableFolder(String folderName) {
        return folderName.contains("drawable");
    }

    public static Set<String> getAllPngResourcesFromFolder(String drawableFolder) {

        File resFolder = new File(drawableFolder);
        File[] childFiles = resFolder.listFiles(new PngFileFilter());
        return getFileNames(childFiles);
    }

    public static Set<String> getAllWebpResourcesFromFolder(String drawableFolder) {

        File resFolder = new File(drawableFolder);
        File[] childFiles = resFolder.listFiles(new WebpFileFilter());
        return getFileNames(childFiles);
    }

    public static Set<String> getAllXmlpResourcesFromFolder(String drawableFolder) {

        File resFolder = new File(drawableFolder);
        File[] childFiles = resFolder.listFiles(new XmlFileFilter());
        return getFileNames(childFiles);
    }

    public static Set<String> getAllJpgResourcesFromFolder(String drawableFolder) {

        File resFolder = new File(drawableFolder);
        File[] childFiles = resFolder.listFiles(new JpegFilter());
        return getFileNames(childFiles);
    }

    public static Set<String> findAllXmlDrawableReferencedBitmaps(String resourceFolderPath) throws Exception {

        final String DRAWABLE_PREFIX = "@drawable/";

        File resFolder = new File(resourceFolderPath);
        File[] drawableFolders = resFolder.listFiles(new FilenameFilter() {
            @Override
            public boolean accept(File dir, String name) {
                return name.startsWith("drawable");
            }
        });

        Set<String> bitmapDrawableNames = new HashSet<>();

        for (File drawableFolder : drawableFolders) {

            File[] xmlDrawables = drawableFolder.listFiles(new FilenameFilter() {
                @Override
                public boolean accept(File dir, String name) {
                    return name.endsWith(".xml");
                }
            });

            for (File xmlDrawableFile : xmlDrawables) {

                String fileContents = IOUtils.toString(new BufferedInputStream(new FileInputStream(xmlDrawableFile)), Charset.defaultCharset());

                String[] lines = fileContents.split("\n");

                for (String line : lines) {

                    if (line.contains(DRAWABLE_PREFIX)) {

                        int startIndex = line.indexOf(DRAWABLE_PREFIX);
                        int endIndex = line.indexOf("\"", startIndex);

                        String drawableName = line.substring(startIndex + DRAWABLE_PREFIX.length(), endIndex);

                        bitmapDrawableNames.add(drawableName);
                    }
                }
            }

            for (File xmlDrawableFile : xmlDrawables) {
                String xmlDrawableFileName = xmlDrawableFile.getName();
                String xmlDrawableName = xmlDrawableFileName.substring(0, xmlDrawableFileName.lastIndexOf('.'));
                bitmapDrawableNames.remove(xmlDrawableName);
            }
        }

        return bitmapDrawableNames;
    }

    public static BufferedImage createPlaceHolder(ImageDimension imageDimension) {

        int sizeX = imageDimension.getWidth();
        int sizeY = imageDimension.getHeight();

        Color transparentColor = new Color(0.0f, 0.0f, 0.0f, 0.0f);

        final BufferedImage bufferedImage = new BufferedImage(sizeX, sizeY, BufferedImage.TYPE_INT_ARGB);
        for (int x = 0; x < sizeX; x++) {
            for (int y = 0; y < sizeY; y++) {
                bufferedImage.setRGB(x, y, transparentColor.getRGB());
            }
        }

        Graphics2D g2d = (Graphics2D) bufferedImage.getGraphics();
        g2d.setComposite(AlphaComposite.SrcOver.derive(0.0f));

        return bufferedImage;
    }

    public static Set<String> getCriticalPngNames() {

        Set<String> criticalBitmapNames = new HashSet<>();

        criticalBitmapNames.add("back_arrow");
        criticalBitmapNames.add("ic_passbook");
        criticalBitmapNames.add("ic_add_money");
        criticalBitmapNames.add("ic_pay_scan_inactive");
        criticalBitmapNames.add("ic_back");
        criticalBitmapNames.add("ic_bank_active");
        criticalBitmapNames.add("ic_bank_inactive");
        criticalBitmapNames.add("ic_cashback_right_swipe");
        criticalBitmapNames.add("ic_home_active");
        criticalBitmapNames.add("ic_home_inactive");
        criticalBitmapNames.add("ic_inbox_active");
        criticalBitmapNames.add("ic_inbox_inactive");
        criticalBitmapNames.add("ic_mall_active");
        criticalBitmapNames.add("ic_mall_inactive");
        criticalBitmapNames.add("ic_pay_with_paytm");
        criticalBitmapNames.add("ic_secure_cta");
        criticalBitmapNames.add("splash_bottom_logos");
        criticalBitmapNames.add("splash_bottom_logos_1");
        criticalBitmapNames.add("splash_bottom_strip");
        criticalBitmapNames.add("splash_center_logo");
        criticalBitmapNames.add("splash_sealtrust");
        criticalBitmapNames.add("splash_text");
        criticalBitmapNames.add("splashscreen_bottom_bank_names");
        criticalBitmapNames.add("ic_paytm_logo");


        return criticalBitmapNames;
    }

    public static void saveAsPng(final BufferedImage bufferedImage, final String path) {
        try {
            ImageIO.write(bufferedImage, "PNG", new File(path));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public static void listAllNestedFiles(File directory, FileFilter fileFilter, List<File> masterList) {

        Stack<File> processStack = new Stack<>();
        processStack.push(directory);

        while (!processStack.empty()) {

            File currDirectory = processStack.pop();

            File[] childEntities = currDirectory.listFiles();

            for (File childEntity : childEntities) {

                if (childEntity.isDirectory()) {
                    processStack.push(childEntity);
                } else {
                    if (fileFilter.accept(childEntity)) {
                        masterList.add(childEntity);
                    }
                }
            }
        }
    }

//    public static void listAllNestedFiles(File directory, FileFilter fileFilter, List<File> masterList) {
//
//        File[] childEntities = directory.listFiles();
//
//        for (File childEntity : childEntities) {
//
//            if (childEntity.isDirectory()) {
//                listAllNestedFiles(childEntity, fileFilter, masterList);
//            } else {
//                if (fileFilter.accept(childEntity)) {
//                    masterList.add(childEntity);
//                }
//            }
//        }
//    }

    public static <T> Set<T> removeDuplicates(List<T> originalList) {
        return new HashSet<>(originalList);
    }
}
