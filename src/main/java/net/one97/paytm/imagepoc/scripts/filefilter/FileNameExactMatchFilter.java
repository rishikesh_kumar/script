package net.one97.paytm.imagepoc.scripts.filefilter;

import java.io.File;
import java.io.FileFilter;

public class FileNameExactMatchFilter implements FileFilter {

    private String fileName;

    public FileNameExactMatchFilter(String fileName) {
        this.fileName = fileName;
    }

    @Override
    public boolean accept(File file) {
        return file.isFile() && file.getName().equals(fileName);
    }
}
