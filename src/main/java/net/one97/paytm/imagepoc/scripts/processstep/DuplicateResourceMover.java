package net.one97.paytm.imagepoc.scripts.processstep;

import net.one97.paytm.imagepoc.scripts.IProcessStep;
import net.one97.paytm.imagepoc.scripts.filefilter.PngFileFilter;
import net.one97.paytm.imagepoc.scripts.util.Utils;

import java.io.File;
import java.io.FileFilter;
import java.util.HashSet;
import java.util.Set;

public class DuplicateResourceMover implements IProcessStep {

    static final String rootPath = "/Users/Ajitesh/Documents/WorkRelatedProjects/paytm-android-app/travelmodule/src/main/res/";
    static final String drawableFolder = rootPath + "drawable";

    static final String desktopFolderPath = "/Users/Ajitesh/Desktop/imagesbackup/";

    @Override
    public void process() throws Exception {

        Set<String> allHdpiResourceNames = Utils.getAllPngResourcesFromFolder(drawableFolder);
        Set<String> toBeDeleteNames = new HashSet<>();

        File rootFolder = new File(rootPath);
        File[] drawableFolders = rootFolder.listFiles(new FileFilter() {
            @Override
            public boolean accept(File childFile) {
                return Utils.isMainDrawableFolder(childFile.getName());
            }
        });

        for (File drawableFolder : drawableFolders) {

            File[] childFiles = drawableFolder.listFiles(new PngFileFilter());
            Set<String> childFileNames = Utils.getFileNames(childFiles);

            for (String childFileName : childFileNames) {

                if (allHdpiResourceNames.contains(childFileName)) {
                    toBeDeleteNames.add(childFileName);
                }
            }
        }

        File desktopFolder = new File(desktopFolderPath);

        if (!desktopFolder.exists())
            desktopFolder.mkdir();

        for (String toBeDeleted : toBeDeleteNames) {

            String inputFileFullPath = drawableFolder + "/" + toBeDeleted;
            String outputFileFullPath = desktopFolderPath + toBeDeleted;

            File inputFile = new File(inputFileFullPath);
            File outputFile = new File(outputFileFullPath);

            inputFile.renameTo(outputFile);

            System.out.println(toBeDeleted);
        }
    }
}
