package net.one97.paytm.imagepoc.scripts.processstep;

import net.one97.paytm.imagepoc.scripts.IProcessStep;
import net.one97.paytm.imagepoc.scripts.util.Utils;

import java.io.File;
import java.util.Set;

public class DuplicateResourceRemover implements IProcessStep {

    @Override
    public void process() throws Exception {

        final String rootPath = "/Users/Ajitesh/Documents/WorkRelatedProjects/paytm-android-app/travelmodule/src/main/res/";
        final String drawableFolder = rootPath + "drawable";

        Set<String> pngResourceNames = Utils.getAllPngResourcesFromFolder(drawableFolder);
        Set<String> jpegResourceNames = Utils.getAllJpgResourcesFromFolder(drawableFolder);
        Set<String> xmlResourceNames = Utils.getAllXmlpResourcesFromFolder(drawableFolder);
        Set<String> webpResourceNames = Utils.getAllWebpResourcesFromFolder(drawableFolder);

        pngResourceNames = Utils.getFileNameWithoutExtension(pngResourceNames);
        jpegResourceNames = Utils.getFileNameWithoutExtension(jpegResourceNames);
        xmlResourceNames = Utils.getFileNameWithoutExtension(xmlResourceNames);
        webpResourceNames = Utils.getFileNameWithoutExtension(webpResourceNames);


        for (String pngResourceName : pngResourceNames) {

            if (webpResourceNames.contains(pngResourceName)) {
                File toBeDeleted = new File(drawableFolder + '/' + pngResourceName + ".webp");
                toBeDeleted.delete();
            }

            if (xmlResourceNames.contains(pngResourceName)) {
                File toBeDeleted = new File(drawableFolder + '/' + pngResourceName + ".xml");
                toBeDeleted.delete();
            }

            if (jpegResourceNames.contains(pngResourceName)) {
                File toBeDeleted = new File(drawableFolder + '/' + pngResourceName + ".jpg");
                toBeDeleted.delete();
                jpegResourceNames.remove(pngResourceName);
            }
        }

        for (String jpegResourceName : jpegResourceNames) {

            if (webpResourceNames.contains(jpegResourceName)) {
                File toBeDeleted = new File(drawableFolder + '/' + jpegResourceName + ".webp");
                toBeDeleted.delete();
            }

            if (xmlResourceNames.contains(jpegResourceName)) {
                File toBeDeleted = new File(drawableFolder + '/' + jpegResourceName + ".xml");
                toBeDeleted.delete();
            }
        }
    }
}
