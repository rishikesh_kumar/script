package net.one97.paytm.imagepoc.scripts.processstep;

import com.sun.istack.internal.NotNull;
import net.one97.paytm.imagepoc.scripts.IProcessStep;
import net.one97.paytm.imagepoc.scripts.filefilter.CustomResourceActivityFilter;
import net.one97.paytm.imagepoc.scripts.filefilter.JavaFileFilter;
import net.one97.paytm.imagepoc.scripts.filefilter.ManifestFileFilter;
import net.one97.paytm.imagepoc.scripts.util.Utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class CustomContextResourceActivityFinder implements IProcessStep {

    private static final String PAYTM_FOLDER = "/Users/ajitesh/Documents/WorkRelatedProjects/paytm-android-app/Jarvis/";
    private static final String PAYTM_GO_FOLDER = "/Users/ajitesh/Documents/WorkRelatedProjects/paytm-android-go/Jarvis/";
    private static final String ANDROID_NAME_PREFIX = "android:name=\"";

    private static final String CUSTOM_RESOURCE_IMPORT_STATEMENT = "import net.one97.paytm.locale.restring.Restring;";
    private static final String CUSTOM_RESOURCE_CODE_SNIPPET = "\n    @Override\n" +
            "    protected void attachBaseContext(Context newBase) {\n" +
            "        super.attachBaseContext(Restring.wrapContext(newBase));\n" +
            "    }";

    @Override
    public void process() throws Exception {

        File paytmDirectory = new File(PAYTM_FOLDER);
        File paytmGoDirectory = new File(PAYTM_GO_FOLDER);

        Set<File> customActivities = new HashSet<>();
        Set<File> defaultActivities = new HashSet<>();
        getCustomResourceActivitiesAndDefaultResourceActivities(paytmDirectory, customActivities, defaultActivities);

        Set<File> goActivities = getAllActivityFiles(paytmGoDirectory);

        Set<File> toBeModifiedGoActivities = new HashSet<>();

        for (File activity : goActivities) {

            String activityName = activity.getName();
            boolean modify = true;

            for (File defaultActivity : defaultActivities) {

                String defaultActivityName = defaultActivity.getName();

                if (defaultActivityName.equalsIgnoreCase(activityName)) {
                    modify = false;
                    break;
                }
            }

            if (modify) {
                toBeModifiedGoActivities.add(activity);
            }
        }

        for (File file : toBeModifiedGoActivities) {
            injectCustomResourceToActivityFile(file);
        }
    }


    //-----------------------------------------------------------------------------------------------------------------------------------------------


    private static void injectCustomResourceToActivityFile(File activityFile) throws Exception {

        System.out.println(activityFile.getName());

        Path path = Paths.get(activityFile.getAbsolutePath());

        List<String> lines = Files.readAllLines(path);

        int indexOfFirstImportLine = -1;
        int lastIndexOfLastClosingBrace = -1;

        for (int i = 0; i < lines.size(); i++) {

            String line = lines.get(i);


            if (line.contains("import ")) {
                indexOfFirstImportLine = i;
                break;
            }
        }

        for (int i = lines.size() - 1; i >= 0; i--) {

            String line = lines.get(i);

            if (line.contains("}")) {
                lastIndexOfLastClosingBrace = i;
                break;
            }
        }

        lines.add(indexOfFirstImportLine, CUSTOM_RESOURCE_IMPORT_STATEMENT);

        String[] codeSnippetLines = CUSTOM_RESOURCE_CODE_SNIPPET.split("\n");

        for (int i = codeSnippetLines.length - 1; i >= 0; i--) {

            String codeSnippetLine = codeSnippetLines[i];

            lines.add(lastIndexOfLastClosingBrace + 1, codeSnippetLine);
        }

        Files.write(path, lines);
    }

    //-----------------------------------------------------------------------------------------------------------------------------------------------

    private static Set<String> getAllActivityNames(@NotNull File parentDirectory) throws Exception {


        List<File> manifestFiles = new ArrayList<>(20);
        Utils.listAllNestedFiles(parentDirectory, new ManifestFileFilter(), manifestFiles);
        Set<File> uniqueManifestFiles = Utils.removeDuplicates(manifestFiles);

        return getActivityNamesFromManifestFile(uniqueManifestFiles);
    }

    private static Set<File> getAllActivityFiles(@NotNull File parentDirectory) throws Exception {

        List<File> javaFiles = new ArrayList<>(5000);
        Utils.listAllNestedFiles(parentDirectory, new JavaFileFilter(), javaFiles);
        Set<File> uniqueJavaFiles = Utils.removeDuplicates(javaFiles);

        Set<String> allActivityNames = getAllActivityNames(parentDirectory);
        return getFilesFromActivityNames(uniqueJavaFiles, allActivityNames);
    }

    private static void getCustomResourceActivitiesAndDefaultResourceActivities(@NotNull File parentDirectory,
                                                                                @NotNull Set<File> customResourceAcitivityFiles,
                                                                                @NotNull Set<File> defaultResourceActivityFiles) throws Exception {
        Set<File> allActivityFiles = getAllActivityFiles(parentDirectory);
        separateCustomResourceAndDefaultResourceActivityFiles(allActivityFiles, customResourceAcitivityFiles, defaultResourceActivityFiles);
    }

    private static void separateCustomResourceAndDefaultResourceActivityFiles(@NotNull Set<File> allActivityFiles,
                                                                              @NotNull Set<File> customResourceFiles,
                                                                              @NotNull Set<File> defaultResourceFiles) throws Exception {

        Set<File> customResourceSyncFiles = Collections.synchronizedSet(customResourceFiles);
        Set<File> defaultResourceSyncFiles = Collections.synchronizedSet(defaultResourceFiles);
        CustomResourceActivityFilter customResourceFilter = new CustomResourceActivityFilter();

        List<Callable<Void>> tasks = new ArrayList<>();

        for (File activityFile : allActivityFiles) {

            tasks.add(() -> {

                if (customResourceFilter.accept(activityFile)) {
                    customResourceSyncFiles.add(activityFile);
                } else {
                    defaultResourceSyncFiles.add(activityFile);
                }

                return null;
            });
        }

        ExecutorService threadPool = Executors.newFixedThreadPool(20);
        threadPool.invokeAll(tasks);
        threadPool.shutdown();
    }

    //-----------------------------------------------------------------------------------------------------------------------------------------------

    private static Set<File> getFilesFromActivityNames(Set<File> allJavaFiles, Set<String> activityNames) throws Exception {

        List<File> matchFiles = Collections.synchronizedList(new ArrayList<>(activityNames.size() * 2));

        List<Callable<Void>> tasks = new ArrayList<>();

        for (String activityName : activityNames) {

            tasks.add(() -> {

                String fileName = activityName + ".java";

                for (File javaFile : allJavaFiles) {
                    if (javaFile.getName().equals(fileName)) {
                        matchFiles.add(javaFile);
                    }
                }
                return null;
            });
        }


        ExecutorService threadPool = Executors.newFixedThreadPool(20);
        threadPool.invokeAll(tasks);
        threadPool.shutdown();

        return Utils.removeDuplicates(matchFiles);
    }

    //-----------------------------------------------------------------------------------------------------------------------------------------------
    private static Set<String> getActivityNamesFromManifestFile(Set<File> manifestFiles) throws Exception {

        Set<String> allActivityNames = new HashSet<>();

        for (File manifestFile : manifestFiles) {

            Set<String> activityNames = getActivityNamesFromManifestFile(manifestFile);
            allActivityNames.addAll(activityNames);
        }

        return allActivityNames;
    }

    private static Set<String> getActivityNamesFromManifestFile(File manifestFile) throws Exception {

        Set<String> activityNames = new HashSet<>();

        try (BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(new FileInputStream(manifestFile)))) {

            String line = null;
            String prevLine = null;

            while ((line = bufferedReader.readLine()) != null) {

                int indexOfName = line.indexOf(ANDROID_NAME_PREFIX);

                if (indexOfName > -1) {

                    if (prevLine.contains("<activity")) {

                        int quoteSearchStartIndex = indexOfName + ANDROID_NAME_PREFIX.length();
                        int indexOfQuote = line.indexOf("\"", quoteSearchStartIndex);
                        String activityQualifiedName = line.substring(quoteSearchStartIndex, indexOfQuote);

                        String activityName = activityQualifiedName.substring(activityQualifiedName.lastIndexOf(".") + 1);
                        activityNames.add(activityName);
                    }
                }

                prevLine = line;
            }
        }

        return activityNames;
    }
}
