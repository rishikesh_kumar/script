package net.one97.paytm.imagepoc.scripts;

import net.one97.paytm.imagepoc.scripts.processstep.ConsolidatePngDrawableMerger;
import net.one97.paytm.imagepoc.scripts.processstep.CustomContextResourceActivityFinder;

public class App {

    public static void main(String[] args) throws Exception {

        new MultiStepProcessor()
                .add(new CustomContextResourceActivityFinder())
                .add(new ConsolidatePngDrawableMerger())
                .process();
    }
}
