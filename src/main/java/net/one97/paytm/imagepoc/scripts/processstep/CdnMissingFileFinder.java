package net.one97.paytm.imagepoc.scripts.processstep;

import net.one97.paytm.imagepoc.scripts.IProcessStep;
import net.one97.paytm.imagepoc.scripts.filefilter.PngFileFilter;
import org.apache.commons.io.IOUtils;

import java.io.*;
import java.net.URL;
import java.util.*;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class CdnMissingFileFinder implements IProcessStep {

    @Override
    public void process() throws Exception {

        final String cdnBaseUrl = "http://d3tn71ud2346e9.cloudfront.net/appimages/";
        final String mergedImageDirectoryPath = "/Users/ajitesh/Desktop/MergedPngDrawables/";
        final String outputPath = "/Users/ajitesh/Desktop/MissingPngs/";

        File mergedImageDirectory = new File(mergedImageDirectoryPath);
        File[] pngFiles = mergedImageDirectory.listFiles(new PngFileFilter());

        File outputDirectory = new File(outputPath);
        if(!outputDirectory.exists())
            outputDirectory.mkdir();

        final Set<String> missingFileNames = Collections.synchronizedSet(new HashSet<>());

        ExecutorService threadPool = Executors.newFixedThreadPool(50);
        List<Callable<Boolean>> tasks = new ArrayList<>();

        for(File childFile : pngFiles){

            tasks.add(() -> {

                String pngFileName = childFile.getName();

                try {

                    URL url = new URL(cdnBaseUrl + pngFileName);
                    InputStream networkInputStream = url.openStream();
                    networkInputStream.close();

                    return true;

                }catch (IOException ex){
                    //ex.printStackTrace();

                    missingFileNames.add(pngFileName);
                    System.out.println(pngFileName);

                    FileInputStream fileInputStream = new FileInputStream(childFile);
                    FileOutputStream fileOutputStream = new FileOutputStream(outputPath + pngFileName);

                    IOUtils.copy(fileInputStream, fileOutputStream);
                    fileOutputStream.flush();

                    fileInputStream.close();
                    fileOutputStream.close();
                }

                return false;
            });
        }

        threadPool.invokeAll(tasks);

        threadPool.shutdown();
    }
}
